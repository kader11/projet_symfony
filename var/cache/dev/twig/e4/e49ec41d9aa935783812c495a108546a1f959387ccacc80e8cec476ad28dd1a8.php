<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* projet_info/index.html.twig */
class __TwigTemplate_9fbb311fab76877c8ca6c73212e45d58a37e661db79aac4a91534a500d91604a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "projet_info/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "projet_info/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "projet_info/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello ProjetInfoController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #000000; padding: 2px 6px; }
</style>

<div class=\"example-wrapper\">
    <div>
    <img src=\"http://www.image-gratuite.com/photos-gratuites/preview/rose-rose.jpg\" alt=\"\" />
    

<ul>
 ";
        // line 17
        echo "  
    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 19
            echo "        <div class=\"p-3 mb-2 bg-success text-black\">
        
        <li>Nom: ";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nom", [], "any", false, false, false, 21), "html", null, true);
            echo "</li>
        <li> Prenom: ";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "prenom", [], "any", false, false, false, 22), "html", null, true);
            echo "</li>
        <div class=\"p-3 mb-2 bg-success text-black\">
        ";
            // line 24
            echo "  
        ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["a"], "coordonnees", [], "any", false, false, false, 25));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 26
                echo "            <li>Email: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "email", [], "any", false, false, false, 26), "html", null, true);
                echo "</li>  
            <li> Adresse: ";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "adresse", [], "any", false, false, false, 27), "html", null, true);
                echo "</li> 
            <li> telephone:";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "telephone", [], "any", false, false, false, 28), "html", null, true);
                echo "</li>
            <li> <a href=\"";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "reseauxSociaux", [], "any", false, false, false, 29), "html", null, true);
                echo "\">Lien Linkedin</a></li>  
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo " 
        </div>
        
        </div>
        ";
            // line 34
            echo " 
        <h1>La technologie Favoris</h1>
        ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["a"], "technoFavoris", [], "any", false, false, false, 36));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 37
                echo "            <li> ";
                echo twig_escape_filter($this->env, $context["b"], "html", null, true);
                echo "</li>    
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "   
        <h1>les langues favoris</h1>
        ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["a"], "langueFavoris", [], "any", false, false, false, 40));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 41
                echo "        
            <li> ";
                // line 42
                echo twig_escape_filter($this->env, $context["b"], "html", null, true);
                echo "</li>    
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo " 

        <h1>Projet informatique</h1>
        ";
            // line 46
            echo " 
        ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["a"], "projetInformatiques", [], "any", false, false, false, 47));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 48
                echo "            <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "nom", [], "any", false, false, false, 48), "html", null, true);
                echo "</li> 
            <li><a href=\"";
                // line 49
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "lien", [], "any", false, false, false, 49), "html", null, true);
                echo "\">lien: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "lien", [], "any", false, false, false, 49), "html", null, true);
                echo "</a></li>
        
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo " 

        <h1>les Hobbie</h1>
        ";
            // line 54
            echo " 
        ";
            // line 55
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["a"], "hobbies", [], "any", false, false, false, 55));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 56
                echo "        
            <li> nom :";
                // line 57
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "nom", [], "any", false, false, false, 57), "html", null, true);
                echo "</li> 
            <li> Description: ";
                // line 58
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "description", [], "any", false, false, false, 58), "html", null, true);
                echo "</li>    
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo " 

    

         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "  
    
</ul>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "projet_info/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 64,  246 => 59,  238 => 58,  234 => 57,  231 => 56,  227 => 55,  224 => 54,  219 => 51,  208 => 49,  203 => 48,  199 => 47,  196 => 46,  191 => 43,  183 => 42,  180 => 41,  176 => 40,  172 => 38,  163 => 37,  159 => 36,  155 => 34,  149 => 30,  141 => 29,  137 => 28,  133 => 27,  128 => 26,  124 => 25,  121 => 24,  116 => 22,  112 => 21,  108 => 19,  104 => 18,  101 => 17,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello ProjetInfoController!{% endblock %}

{% block body %}
<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #000000; padding: 2px 6px; }
</style>

<div class=\"example-wrapper\">
    <div>
    <img src=\"http://www.image-gratuite.com/photos-gratuites/preview/rose-rose.jpg\" alt=\"\" />
    

<ul>
 {#   je parcoure le tableau pour recuperer le nom et le prenom de l'utilisateur #}  
    {% for a in users %}
        <div class=\"p-3 mb-2 bg-success text-black\">
        
        <li>Nom: {{a.nom}}</li>
        <li> Prenom: {{a.prenom}}</li>
        <div class=\"p-3 mb-2 bg-success text-black\">
        {#   je parcoure les coordonnées de chaque utilisateur pour recuperer ces coordonnées #}  
        {% for b in a.coordonnees %}
            <li>Email: {{b.email}}</li>  
            <li> Adresse: {{b.adresse}}</li> 
            <li> telephone:{{b.telephone}}</li>
            <li> <a href=\"{{b.reseauxSociaux}}\">Lien Linkedin</a></li>  
        {% endfor %} 
        </div>
        
        </div>
        {#   je parcoure les technologies Favoris de chaque utilisateur p #} 
        <h1>La technologie Favoris</h1>
        {% for b in a.technoFavoris %}
            <li> {{b}}</li>    
        {% endfor %}   
        <h1>les langues favoris</h1>
        {% for b in a.langueFavoris %}
        
            <li> {{b}}</li>    
        {% endfor %} 

        <h1>Projet informatique</h1>
        {#   je parcoure projets Informatiques Favoris de chaque etulisateur p #} 
        {% for b in a.projetInformatiques %}
            <li>{{b.nom}}</li> 
            <li><a href=\"{{b.lien}}\">lien: {{b.lien}}</a></li>
        
        {% endfor %} 

        <h1>les Hobbie</h1>
        {#   je parcoure les hobbies de chaque etulisateur #} 
        {% for b in a.hobbies %}
        
            <li> nom :{{b.nom}}</li> 
            <li> Description: {{b.description}}</li>    
        {% endfor %} 

    

         
    {% endfor %}  
    
</ul>
</div>
</div>
{% endblock %}


", "projet_info/index.html.twig", "/home/e20160012156/Bureau/projet_symfony/projet_symfony/Projet_symfony-master/templates/projet_info/index.html.twig");
    }
}
