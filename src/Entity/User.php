<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="array")
     */
    private $technoFavoris = [];

    /**
     * @ORM\Column(type="array")
     */
    private $langueFavoris = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hobbie", mappedBy="user")
     */
    private $hobbies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjetInformatique", mappedBy="user")
     */
    private $projetInformatiques;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cordonnee", mappedBy="user")
     */
    private $coordonnees;

    public function __construct()
    {
        $this->hobbies = new ArrayCollection();
        $this->projetInformatiques = new ArrayCollection();
        $this->coordonnees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTechnoFavoris(): ?array
    {
        return $this->technoFavoris;
    }

    public function setTechnoFavoris(array $technoFavoris): self
    {
        $this->technoFavoris = $technoFavoris;

        return $this;
    }

    public function getLangueFavoris(): ?array
    {
        return $this->langueFavoris;
    }

    public function setLangueFavoris(array $langueFavoris): self
    {
        $this->langueFavoris = $langueFavoris;

        return $this;
    }

    /**
     * @return Collection|Hobbie[]
     */
    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function addHobby(Hobbie $hobby): self
    {
        if (!$this->hobbies->contains($hobby)) {
            $this->hobbies[] = $hobby;
            $hobby->setUser($this);
        }

        return $this;
    }

    public function removeHobby(Hobbie $hobby): self
    {
        if ($this->hobbies->contains($hobby)) {
            $this->hobbies->removeElement($hobby);
            // set the owning side to null (unless already changed)
            if ($hobby->getUser() === $this) {
                $hobby->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProjetInformatique[]
     */
    public function getProjetInformatiques(): Collection
    {
        return $this->projetInformatiques;
    }

    public function addProjetInformatique(ProjetInformatique $projetInformatique): self
    {
        if (!$this->projetInformatiques->contains($projetInformatique)) {
            $this->projetInformatiques[] = $projetInformatique;
            $projetInformatique->setUser($this);
        }

        return $this;
    }

    public function removeProjetInformatique(ProjetInformatique $projetInformatique): self
    {
        if ($this->projetInformatiques->contains($projetInformatique)) {
            $this->projetInformatiques->removeElement($projetInformatique);
            // set the owning side to null (unless already changed)
            if ($projetInformatique->getUser() === $this) {
                $projetInformatique->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cordonnee[]
     */
    public function getCoordonnees(): Collection
    {
        return $this->coordonnees;
    }

    public function addCoordonnee(Cordonnee $coordonnee): self
    {
        if (!$this->coordonnees->contains($coordonnee)) {
            $this->coordonnees[] = $coordonnee;
            $coordonnee->setUser($this);
        }

        return $this;
    }

    public function removeCoordonnee(Cordonnee $coordonnee): self
    {
        if ($this->coordonnees->contains($coordonnee)) {
            $this->coordonnees->removeElement($coordonnee);
            // set the owning side to null (unless already changed)
            if ($coordonnee->getUser() === $this) {
                $coordonnee->setUser(null);
            }
        }

        return $this;
    }
}
